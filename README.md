# Drupal-Module-Social-Media

This social media module helps integrate your website with social media sites such as Twitter, Facebook, Google+ and Foursquare. It provides an centralized way of managing social media profile information.

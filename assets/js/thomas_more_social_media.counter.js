(function ($) {

    $('.social-media a').on('click', function() {
        var network = $(this).data( "network" );
        $.ajax({
            type: "POST",
            data: { 
                network: network
            },
            url: "/ajax/thomas-more-social-media/counter",
            success: function(resp) {
                var elementClass = "."+resp+" span";
                var count = parseInt($(elementClass).text()) + 1;
                $(elementClass).text(count);
            },
            error: function() {
                console.log('error');
            }
        });
    });
    
  }(jQuery));
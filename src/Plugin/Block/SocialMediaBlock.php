<?php

namespace Drupal\thomas_more_social_media\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Database\Connection;

/**
 * Provides a 'Social Media Thomas More' Block.
 *
 * @Block(
 *   id = "thomas_more_social_media_block",
 *   admin_label = @Translation("Social Media Thomas More"),
 *   category = @Translation("Social Media Thomas More"),
 * )
 */
class SocialMediaBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
        return [
            '#theme'    =>  'social-media',
            '#attached' => array(
              'library' => array(
                'thomas_more_social_media/social_media',
              ),
            ),
            '#facebook_url' => \Drupal::state()->get('facebook_url'),
            '#google_url' => \Drupal::state()->get('google_url'),
            '#twitter_url' => \Drupal::state()->get('twitter_url'),
            '#linkedin_url' => \Drupal::state()->get('linkedin_url'),
            '#foursquare_url' => \Drupal::state()->get('foursquare_url'),
            '#facebook_count' => $this->getSocialMediaCount('facebook'),
            '#google_count' => $this->getSocialMediaCount('google'),
            '#twitter_count' => $this->getSocialMediaCount('twitter'),
            '#linkedin_count' => $this->getSocialMediaCount('linkedin'),
            '#foursquare_count' => $this->getSocialMediaCount('foursquare')
        ];
  }

  public function getSocialMediaCount($network) {
    // Create an object of type Select
    $query = \Drupal::database()->select('social_media_data', 's');
    // Add extra detail to this query object: a condition, fields and a range
    $query->condition('s.network', $network, '=');
    $result = $query->countQuery()->execute()->fetchField();
    return $result;
  }

}
<?php

namespace  Drupal\thomas_more_social_media\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class SettingsForm extends FormBase {
    public function getFormId() {
        return 'thomas_more_social_media_form';
    }

    public function buildForm(array $form, FormStateInterface $form_state) {

        $form['facebook_url'] = [
            '#type' => 'textfield',
            '#title' => 'Facebook',
            '#default_value' => \Drupal::state()->get('facebook_url')
        ];

        $form['google_url'] = [
            '#type' => 'textfield',
            '#title' => 'Google',
            '#default_value' => \Drupal::state()->get('google_url')
        ];

        $form['twitter_url'] = [
            '#type' => 'textfield',
            '#title' => 'Twitter',
            '#default_value' => \Drupal::state()->get('twitter_url')
        ];

        $form['linkedin_url'] = [
            '#type' => 'textfield',
            '#title' => 'Linkedin',
            '#default_value' => \Drupal::state()->get('linkedin_url')
        ];

        $form['foursquare_url'] = [
            '#type' => 'textfield',
            '#title' => 'Foursquare',
            '#default_value' => \Drupal::state()->get('foursquare_url')
        ];
              
        $form['submit'] = [
            '#type' => 'submit',
            '#value' => $this->t('Submit'),
        ];
        return $form;
    }

    public function submitForm(array &$form, FormStateInterface $form_state) {
        \Drupal::state()->set('facebook_url', $form['facebook_url']['#value']);
        \Drupal::state()->set('twitter_url', $form['twitter_url']['#value']);
        \Drupal::state()->set('linkedin_url', $form['linkedin_url']['#value']);
        \Drupal::state()->set('google_url', $form['google_url']['#value']);
        \Drupal::state()->set('foursquare_url', $form['foursquare_url']['#value']);
    }
}
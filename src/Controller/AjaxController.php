<?php

namespace  Drupal\thomas_more_social_media\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Drupal\Core\Database\Connection;

class AjaxController extends ControllerBase {
    public function counter(Request $request) {
        $permission = "skip tracking clicks";
        $user = \Drupal::currentUser();
        if($user->hasPermission($permission)) {
            $time_clicked=\Drupal::time()->getRequestTime();
            $network = $request->get('network');
            $result = \Drupal::database()->insert('social_media_data')
                ->fields([
                    'network'       =>  $network,
                    'time_clicked'  =>  $time_clicked
                ])
                ->execute();
            return new Response($request->get('network'));
        }
        return new Response('not ok');
    }
}
